package net.vikesh.ce.domain;

/**
 * @auth Vikesh
 */
public enum AddressType {
    HOME,
    BILLING,
    SHIPPING,
    OFFICE,
    OTHER
}
