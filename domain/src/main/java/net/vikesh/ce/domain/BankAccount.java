package net.vikesh.ce.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @auth Vikesh
 */
@Entity
@Table(name = "BANK_ACCOUNT")
public class BankAccount extends BillingDetails {
    private static final Logger LOG = LoggerFactory.getLogger(BankAccount.class);

    @Column(name = "BANK_ACCOUNT_NUMBER")
    private String number;

    @Column(name = "BANK_ACCOUNT_NAME")
    private String bankName;

    @Column(name = "BANK_ACCOUNT_SWIFT")
    private String swift;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "number='" + number + '\'' +
                ", bankName='" + bankName + '\'' +
                ", swift='" + swift + '\'' +
                '}';
    }
}
