package net.vikesh.ce.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @auth Vikesh
 */
@Entity
@Table(name = "BID")
public class Bid extends Item{
    private static final Logger LOG = LoggerFactory.getLogger(Bid.class);

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @OneToOne(optional = false)
    private User user;

    @OneToOne(optional = false)
    private BidItem bidItem;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BidItem getBidItem() {
        return bidItem;
    }

    public void setBidItem(BidItem bidItem) {
        this.bidItem = bidItem;
    }

    @Override
    public String toString() {
        return "Bid{" +
                "amount=" + amount +
                ", user=" + user +
                ", bidItem=" + bidItem +
                '}';
    }
}
