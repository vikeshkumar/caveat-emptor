package net.vikesh.ce.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Set;

/**
 * @auth Vikesh
 */
@Entity(name = "BID_ITEM")
public class BidItem extends Item {
    private static final Logger LOG = LoggerFactory.getLogger(BidItem.class);

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "INITIAL_PRICE")
    private BigDecimal initialPrice;

    @Column(name = "RESERVE_PRICE")
    private BigDecimal reservePrice;

    @Column(name = "BID_START_DATE_TIME")
    private Instant startDate;

    @Column(name = "BID_END_DATE_TIME")
    private Instant endDate;

    @Column(name = "BID_STATE")
    @Enumerated(EnumType.STRING)
    private BidItemState state;

    @Column(name = "APPROVAL_DATE_TIME")
    private Instant approvalDateTime;

    @OneToMany
    private Set<Category> category;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(BigDecimal initialPrice) {
        this.initialPrice = initialPrice;
    }

    public BigDecimal getReservePrice() {
        return reservePrice;
    }

    public void setReservePrice(BigDecimal reservePrice) {
        this.reservePrice = reservePrice;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public BidItemState getState() {
        return state;
    }

    public void setState(BidItemState state) {
        this.state = state;
    }

    public Instant getApprovalDateTime() {
        return approvalDateTime;
    }

    public void setApprovalDateTime(Instant approvalDateTime) {
        this.approvalDateTime = approvalDateTime;
    }

    public Set<Category> getCategory() {
        return category;
    }

    public void setCategory(Set<Category> category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "BidItem{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", initialPrice=" + initialPrice +
                ", reservePrice=" + reservePrice +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", state=" + state +
                ", approvalDateTime=" + approvalDateTime +
                ", category=" + category +
                '}';
    }
}
