package net.vikesh.ce.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @auth Vikesh
 */
public enum BidItemState {
    SOLD,
    QUEUED,
    ACTIVE,

}
