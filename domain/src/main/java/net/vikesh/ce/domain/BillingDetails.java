package net.vikesh.ce.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @auth Vikesh
 */
@Table(name = "BILLING_DETAILS")
public class BillingDetails extends Item {
    private static final Logger LOG = LoggerFactory.getLogger(BillingDetails.class);

    @Column(name = "OWNER")
    private String owner;


    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "BillingDetails{" +
                "owner='" + owner + '\'' +
                '}';
    }
}
