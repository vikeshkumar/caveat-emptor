package net.vikesh.ce.domain;

import javax.persistence.*;

/**
 * @auth Vikesh
 */
@Entity
@Table(name = "COMMENTS")
public class Comment extends Item {

    @OneToOne(optional = false)
    @JoinColumn(name = "ID", nullable = false, updatable = false)
    private BidItem commentFor;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "RATING")
    @Enumerated(EnumType.STRING)
    private Rating rating;

    public BidItem getCommentFor() {
        return commentFor;
    }

    public void setCommentFor(BidItem commentFor) {
        this.commentFor = commentFor;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commentFor=" + commentFor +
                ", text='" + text + '\'' +
                ", rating=" + rating +
                '}';
    }
}
