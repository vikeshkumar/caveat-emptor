package net.vikesh.ce.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;

/**
 * @auth Vikesh
 */
@Entity
@Table(name = "CREDIT_CARD")
public class CreditCard extends BillingDetails {
    private static final Logger LOG = LoggerFactory.getLogger(CreditCard.class);

    @Enumerated(EnumType.STRING)
    private CreditCardType creditCardType;

    @Column(name = "NUMBER")
    private String number;

    @Column(name = "EXP_MONTH")
    private String expMonth;

    @Column(name = "EXP_YEAR")
    private String expYear;

    @Column(name = "CVV")
    private String cvv;

    public CreditCardType getCreditCardType() {
        return creditCardType;
    }

    public void setCreditCardType(CreditCardType creditCardType) {
        this.creditCardType = creditCardType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    public String getExpYear() {
        return expYear;
    }

    public void setExpYear(String expYear) {
        this.expYear = expYear;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }
}
