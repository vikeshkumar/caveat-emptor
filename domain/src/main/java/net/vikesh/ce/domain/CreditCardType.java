package net.vikesh.ce.domain;

/**
 * @auth Vikesh
 */
public enum CreditCardType {
    MASTER,
    VISA,
    ELECTRON,
    RUPIAH
}
