package net.vikesh.ce.domain;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

/**
 * @auth Vikesh
 */
@MappedSuperclass
public class Item {
    private static final Logger LOG = LoggerFactory.getLogger(Item.class);

    @Id
    @Column(name = "ID", length = 36)
    private String uuid;

    @Column(name = "CREATED")
    private Instant created;

    @Column(name = "UPDATED")
    private Instant updated;

    public String getUuid() {
        return uuid;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    @PrePersist
    public void beforeCreate() {
        if (created == null) {
            created = Instant.now();
        }

        if (StringUtils.isBlank(uuid)) {
            uuid = UUID.randomUUID().toString();
        }
    }

    @PreUpdate
    public void beforeUpdate() {
        updated = Instant.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return uuid.equals(item.uuid);

    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }
}
