package net.vikesh.ce.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @auth Vikesh
 */
public enum Rating {
    ZERO,
    ONE,
    ONE_HALF,
    TWO,
    TWO_HALF,
    THREE,
    THREE_HALF,
    FOUR_HALF,
    FIVE
}
