package net.vikesh.ce.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * @auth Vikesh
 */
public class Shipment extends Item {
    private static final Logger LOG = LoggerFactory.getLogger(Shipment.class);

    @Column(name = "INSPECTION_PERIOD")
    private Integer inspectionPeriod;

    @Enumerated(EnumType.STRING)
    private ShipmentState shipmentState;

    @Column(name = "BUYER")
    private User buyer;

    @Column(name = "SELLER")
    private User seller;

    @Column(name = "DELIVERY_ADDRESS")
    private Address deliveryAddress;

    public Integer getInspectionPeriod() {
        return inspectionPeriod;
    }

    public void setInspectionPeriod(Integer inspectionPeriod) {
        this.inspectionPeriod = inspectionPeriod;
    }

    public ShipmentState getShipmentState() {
        return shipmentState;
    }

    public void setShipmentState(ShipmentState shipmentState) {
        this.shipmentState = shipmentState;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    @Override
    public String toString() {
        return "Shipment{" +
                "inspectionPeriod=" + inspectionPeriod +
                ", shipmentState=" + shipmentState +
                ", buyer=" + buyer +
                ", seller=" + seller +
                ", deliveryAddress=" + deliveryAddress +
                '}';
    }
}
