package net.vikesh.ce.domain;

/**
 * @auth Vikesh
 */
public enum ShipmentState {
    YET_TO_BE_DISPATCHED,
    IN_TRANSIT,
    AT_NEAREST_HUB,
    OUT_FOR_DELIVERY
}
